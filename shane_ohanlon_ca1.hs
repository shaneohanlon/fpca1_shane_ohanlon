import qualified Data.List as IntersectList
import Data.CaseInsensitive (foldCase)

-- Shane O' Hanlon CA1 (Functional Programming)

-- Question: 1
-- Function: Adds two numbers and doubles result
-- Comments: Forgot to add the '()' which resulted in it not working correctly.
add_and_double :: (Num a) => a->a->a
add_and_double x y = (x + y) * 2

-- Question: 2
-- Function: Write add_and_double using infix operator.
-- Comments: 
(+*) :: (Num a) => a -> a -> a
(+*) a b = a `add_and_double` b

-- Question: 3
-- Function: Write solve_quadratic_equation taking three arguements.
-- Comments: Help from https://gist.github.com/rbtbr/243360
solve_quadratic_equation :: Double -> Double -> Double -> (Double,Double)
solve_quadratic_equation a b c = (x1, x2)
    where
    x1 = e + sqrt (root /(2 * a))
    x2 = e - sqrt (root / (2 * a))
    root = (b * b) - 4 * a * c 
    e = - b / (2 / a)

-- Question: 4
-- Function: first_n which takes an Int value (n) and returns a list of the first n Ints starting from 1.
-- Comments: 
first_n :: Int -> [Int]
first_n  n 
    | n < 0 = error"Must be positive"
    | otherwise = take n [1..]

-- Question: 5
-- Function: Re-write first_n ,which will take an Integer argument and return a list of Integers.
-- Comments: 
first_n_integers :: Integer -> [Integer]
first_n_integers n = take_integer n [1..]
        where take_integer :: Integer -> [Integer] -> [Integer]
              take_integer n (x:xs)
                  |n < 0 = error "Negative Value"
                  |n == 0 = []
                  |otherwise = x : take_integer (n - 1) xs

-- Question: 6
-- Function: Write double_factorial.Take an Integer and computes factorial.
-- Comments: Rewrote it to product_factorial 
product_factorial::Integer -> Integer
product_factorial n 
    | n <= 0 = 1 
    | otherwise = factorial (n) * product_factorial (n-1)
         where 
         factorial :: Integer -> Integer
         factorial n
                | n == 0 = 1
                | otherwise = n * factorial (n - 1) 

-- Question: 7
-- Function: Define an infinite list of factorials called factorials using the zipWith function
-- Comments: Oshi Kavadia helped with fixing errors with this one.
factorial :: Integer -> [Integer]
factorial n
        |n < 0 = []
        |n == 0 = []
        |otherwise =  1 : zipWith (*) [1..] (factorial (n - 1))

-- Question: 8
-- Function: Write isPrime :: Integer -> Bool .
-- Comments: Help from "http://stackoverflow.com/questions/4541415/haskell-prime-test"
isPrime:: Integer -> Bool
isPrime x = null $ filter (\y ->  x `mod` y == 0) $ takeWhile (\y ->  y*y <= x) [2..]

-- Question: 9
-- Function: Define prime which returns a list of primes.
-- Comments: 
primes::[Integer]
primes = filter isPrime [0..]

-- Question: 10
-- Function: Write a function to sum a list of integers recursively
-- Comments: 
sumList :: [Int] -> Int
sumList [] = 0
sumList (x:xs) = x + sumList xs

-- Question: 11
-- Function: Write a function to sum a list of integers in terms of foldl.
-- Comments: 
sumListFL :: [Int] -> Int
sumListFL = foldl (+) 0

-- Question: 12
-- Function: Write a function to multiply a list of integers in terms of foldr.
-- Comments: 
multList :: [Int] -> Int
multList = foldr (*) 1

-- Question: 13
-- Function: Write a guessing game.
-- Comments: Was looking for ways to compare and found this. It does return string in lower case though but
-- but it just used it to compare, capitals in input is fine. https://hackage.haskell.org/package/case-insensitive-1.2.0.5/docs/Data-CaseInsensitive.html
guess :: String -> Int -> String
guess a b
        |foldCase a == "i love functional programming" && b < 5  = "You have won"
        |foldCase a /= "i love functional programming" && b < 5 = "Guess again"
        |otherwise = "you have lost"

-- Question: 14
-- Function: Write dot product of vector
-- Comments: 
dotProduct :: [Integer] -> [Integer] -> Integer
dotProduct x y | length x == length y = sum (zipWith (*) x y)
         | otherwise = error "Length must be same"

-- Question: 15
-- Function: Write a function to check if n is even
-- Comments: 
isEven :: Integer -> Bool
isEven n = if n `mod` 2 == 0 then True  else False

-- Question: 16
-- Function: A function which removes all vowels from a string
-- Comments: Help from (Didnt realise mine was so similar to his when I got stuck and found his)http://stackoverflow.com/questions/7623651/vowel-datatype-in-haskell-is-it-possibleHelp from (Didnt realise mine was so similar to his
unixname :: [Char] -> [Char]
vowel x = elem x "aeiouAEIOU"
unixname [] = []
unixname (x:xs) 
    | vowel x = unixname xs
    | otherwise = x : unixname xs

 --Question: 17
 --Function: A function intersection, which given two lists calculates the intersection
 --Comments: https://www.haskell.org/hoogle/?hoogle=intersect
intersection :: Eq a => [a] -> [a] -> [a]
intersection = IntersectList.intersect