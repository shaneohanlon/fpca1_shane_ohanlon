

-- Question: 1
-- Function: Adds two numbers and doubles result
-- Comments: Forgot to add the '()' which resulted in it not working correctly.
add_and_double :: (Num a) => a->a->a
add_and_double x y = (x + y) * 2


-- Question: 2
-- Function: Write add_and_double using infix operator.
-- Comments: 
(+*) :: (Num a) => a -> a -> a
(+*) a b = a `add_and_double` b

-- Question: 3
-- Function: Write solve_quadratic_equation taking three arguements.
-- Comments: Help from https://gist.github.com/rbtbr/243360
solve_quadratic_equation :: Double -> Double -> Double -> (Double,Double)
solve_quadratic_equation (a, b, c) = (x1, x2)
   where
      x1 = e + e + sqrt root /(2 * a)
      x2 = e - sqrt root / (2 * a)
      root = b * b - 4 * a * c 
      e = - b / (2 / a)

-- Question: 4
-- Function: first_n which takes an Int value (n) and returns a list of the first n Ints starting from 1.
-- Comments:
first_n :: Int -> [Int]
first_n  n 
    | n < 0 = error"Must be positive"
    | otherwise = take n [1..] 

    
    

-- Question: 5
-- Function: Re-write first_n ,which will take an Integer argument and return a list of Integers.
-- Comments: 
first_n_integers :: Integer -> [Integer]
first_n_integers  n 
first_n_integers = take_integers []
    | n < 0 = error"Must be positive"
    | n = [] = error"List must not be empty"
    | otherwise = take_integer n [1..] 

-- Question: 6
-- Function: Write double_factorial.Take an Integer and computes factorial.
-- Comments: Rewrote it to product_factorial 
product_factorial::Integer -> Integer
product_factorial n = factorial[1..n]
    where
    factorial 0 = 1
    factorial n = n * factorial (n - 1)


-- Question: 7
-- Function: Define an infinite list of factorials called factorials using the zipWith function
-- Comments: 
factorial :: [Integer] 
factorial = 1 : zipWith (*) [1..] factorial 

-- Question: 8
-- Function: Write isPrime :: Integer -> Bool .
-- Comments: Help from "http://stackoverflow.com/questions/4541415/haskell-prime-test"
isPrime:: Integer -> Bool
isPrime x = null $ filter (\y ->  x `mod` y == 0) $ takeWhile (\y ->  y*y <= x) [2..]

-- Question: 9
-- Function: Define prime which returns a list of primes.
-- Comments: Looked up the 'isPrime' function. https://hackage.haskell.org/package/primes-0.2.1.0/docs/Data-Numbers-Primes.html
primes::[Integer]
primes = filter isPrime [0..]

-- Question: 10
-- Function: Write a function to sum a list of integers recursively
-- Comments: 
sumList :: [Int] -> Int
sumList [] = 0
sumList (x:xs) = x + sumList xs

-- Question: 11
-- Function: Write a function to sum a list of integers in terms of foldl.
-- Comments: 
sumListFL :: [Int] -> Int
sumListFL = foldl (+) 0

-- Question: 12
-- Function: Write a function to multiply a list of integers in terms of foldr.
-- Comments: 
multList :: [Int] -> Int
multList = foldr (*) 1

-- Question: 13
-- Function: Write a guessing game.
-- Comments:if x > 90 then "You got a A"
guess:: :: Char -> Int
guess x y
    if y < 5 then "I love functional programming"
    else



-- Question: 15
-- Function: Write a function to check if n is even
-- Comments: 
isEven :: Integer -> Bool
isEven n = if n `mod` 2 == 0 then True  else False