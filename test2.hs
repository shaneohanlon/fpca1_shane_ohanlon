import qualified Data.List as IntersectList
import Data.CaseInsensitive (foldCase)
-- Question: 5
-- Function: Re-write first_n ,which will take an Integer argument and return a list of Integers.
-- Comments: 
--first_n_integers :: Integer -> [Integer]
--first_n_integers n = take_integer n [1..]
--        where take_integer :: Integer -> [Integer] -> [Integer]
--              take_integer n (x:xs)
--                  |n < 0 = error "Negative Value"
--                  |n == 0 = []
--                  |otherwise = x : take_integer (n - 1) xs



-- Question: 13
-- Function: Write a guessing game.
-- Comments: Was looking for ways to compare and found this. It does return string in lower case though but
-- but it just used it to compare, capitals in input is fine. https://hackage.haskell.org/package/case-insensitive-1.2.0.5/docs/Data-CaseInsensitive.html
--guess :: String -> Int -> String
--guess a b
--        |foldCase a == "i love functional programming" && b < 5  = "You have won"
--        |foldCase a /= "i love functional programming" && b < 5 = "Guess again"
--        |otherwise = "you have lost"

 --Question: 18
 --Function: Function which will replace all vowels in a string with the letter x
 --Comments: 
--censor :: [Char] -> [Char]
--vowel x = elem x "aeiouAEIOU"
--censor [] = []
--censor (x:xs) 
--  | vowel x = censor xs
--  | otherwise = x : censor xs
